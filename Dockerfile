FROM keymetrics/pm2:14-alpine
ENV NPM_CONFIG_LOGLEVEL warn
WORKDIR /app
COPY . .
VOLUME /app
EXPOSE 4000

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]
