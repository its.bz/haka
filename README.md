# ITS :: Fullstack boilerplate

[![timesheet](https://wakatime.com/badge/user/b63eae7a-adaa-4419-a51e-0f59a1a4a46f/project/72a81e3f-2cc0-47f3-a0ea-5cce071a2965.svg)](https://wakatime.com/projects/haka.its.bz)
[![pipeline status](https://gitlab.com/its.bz/haka/badges/main/pipeline.svg)](https://gitlab.com/its.bz/haka/-/commits/main)
[![coverage report](https://gitlab.com/its.bz/haka/badges/main/coverage.svg)](https://gitlab.com/its.bz/haka/-/commits/main)

Site and mobile application (PWA+cordova) for RK7.CRM by UCS

## Themes

This app may be customized for different brands and builds simultaneously in different ```dist/$theme``` path.\
When build are done you can deploy an accorded path to accorded server to serve it.\
Or use CI for deploy.

## Architecture

App consist of backend part nd frontend part.\
Backend serves front on port on development and from directory as static.\
There is no additional server needed to serve frontend.

## Production

```shell
# Serve app using PM2 supervisor
yarn global add pm2

# Set theme that you want to build
theme=<theme>
./build.sh $theme

# Start application
cd dist/$theme
pm2 start ecosystem.config.js
```

## Development

```shell
(
cd front && yarn install && yarn dev
)
(
cd back && yarn install && yarn dev
)
```

## Docker
You can build docker image using ```Dockerfile``` file and then use it:
```shell
cd dist/<theme>
sudo docker build -t <tag_here> .
docker run -p <your_port>:4100 -v <your_logs_dir>:/app/back/logs/ <tag_here>
```
example:
```shell
cd dist/ostrov
sudo docker build -t osis_crm-ostrov .
# on Linux
docker run -p 4101:4100 -v $(pwd)/logs/:/app/back/logs/ osis_crm-ostrov
# on Windows
docker run -p 4101:4100 -v ./logs/:/app/back/logs/ osis_crm-ostrov
```

## Release
Use ```release.sh``` script to:
- Increment patch block in package versions
- Commit changes, tag commit (need for properly create changelog)
- Update changelog in ```README.md``` file and amment commit with new changes 
- Delete previous tag and tag last commit

For now you can check is all OK and then push changes to remotes with:
```shell
git push origin
```

## Contributing

Project is fully proprietary and not open sourced. Nobody contribute but ITS company employees.

## Bugs

Submit bugs [here](https://gitlab.com/ostrov.team/ostrovis/osis_crm/issues)

## History

```
0.0.11 @ 3 January 2022  
- Исправление скрипта релиза  
- Исправление CI  
- Исправление скрипта релиза  
- Исправление CI

0.0.9 @ 3 January 2022  
- IB-3 Перевод фронта на TS  
- IB-3 Авторизация и регистрация по СМС на фронте  
- Реализовал все обработчки API для логина по SMS, и всё для /users  
- Исправление CI  
- Обновил зависимости для back  
- IB-3 Авторизация и регистрация по СМС на фронте

0.0.8 @ 30 December 2021  
- Исправление автоформатирования кода  
- Реализовал глобальный обработчик ошибок, сделал все эндпоинты для User  
- IB-3 Добавил работу с миграциями БД  
- IB-3 Исправил миграцию таблицы Users  
- Реализовал глобальный обработчик ошибок, сделал все эндпоинты для User  
- Реализовал глобальный обработчик ошибок, сделал все эндпоинты для User  
- IB-3 Исправил запуск миграций БД

0.0.7 @ 30 December 2021  
- Исправление скрипта релиза  
- Исправление скрипта релиза  
- Merge tag '0.0.5' into test  
- Исправление скрипта релиза

0.0.5 @ 30 December 2021  
- Исправление скрипта релиза  
- Merge tag '0.0.4' into test  
- IB-2 Исправление опопвещения при деплое

0.0.4 @ 30 December 2021  
- IB-2 Исправление опопвещения при деплое  
- Merge tag '0.0.3' into test

0.0.3 @ 30 December 2021  
- IB-2 Добавление покрытия тестами кода в GitLab, исправление обновления PM2  
- IB-2 Исправление деплоя  
- IB-2 Добавление покрытия тестами кода в GitLab, исправление обновления PM2

0.0.2 @ 30 December 2021  
- Удалил файл .env с учётными данными из репы, поправил сборку, .gitignore  
- Initial commit  
- Отказ от старого бэка в пользу NestJS  
- Create all routes, auth checking, roles, login-logout functionality  
- IB-2 Исправление тестов  
- IB-2 Добавление покрытия тестами кода в GitLab, исправление обновления PM2  
- Add profile page  
- Реструктуризация проекта и тестов бэка  
- Add dark mode support and dark|light theming, vuex store, logo  
- Add CV examples preview link, QR-code generator, new logo, fix dark mode saving for Worker layout  
- Add test server CI deploy  
- Add Orders form  
- Fix ecosystem.config.json  
- Переписал под ИНН+пароль авторизацию  
- Initial commit  
- Fix db ENV and PM2 deploy  
- Fix wide screen layout, add PWA updater  
- Add Clients and Profile pages. CVs opens profile page.  
- Add Messages form  
- Исправления в CI и сюорке проекта  
- Fix updater in ServiceWorker  
- Revert Users dto and model  
- Fix DB connections, add theme color  
- Исправления в CI  
- Change deploy to 10x clustered back&front instances  
- Fix ServiceWorker  
- Fixing PM2 for back  
- IB-4 Отказ от старого бэка в пользу NestJS  
- Исправления в CI  
- Исправления в CI  
- Final bix backend building  
- Fix CI  
- Fix icongenie  
- IB-2 Исправление CI  
- IB-2 Исправление CI  
- Fix Telegram deploy notification  
- Fix Telegram deploy notification  
- Change deploy message  
- Fix Telegram deploy notification  
- Fix CI build  
- Fix CI

```

## Credits

Contact us: [dev@its.bz](mailto:dev@its.bz)\
Our site: [its.bz](https://its.bz)

## License

Proprietary
