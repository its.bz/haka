// eslint-disable-next-line @typescript-eslint/no-var-requires
const { config: cfg } = require('dotenv')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { config } = require('./src/config.ts')

cfg()
module.exports = {
	type: config().database.type,
	host: config().database.host,
	port: config().database.port,
	username: config().database.user,
	password: config().database.password,
	database: config().database.name,
	entities: [__dirname + '/../**/*.entity{.ts,.js}'],
	synchronize: config().env === 'development',
	migrations: ['src/database/migrations/**/*.ts'],
	cli: {
		entitiesDir: 'src/modules/entity',
		migrationsDir: 'src/database/migrations',
	},
}
