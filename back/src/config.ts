export type DBType =
	| 'mysql'
	| 'mariadb'
	| 'postgres'
	| 'cockroachdb'
	| 'sqlite'
	| 'mssql'

export const config = () => ({
	env: process.env.NODE_ENV || 'development',
	app: {
		port: parseInt(process.env.PORT, 10) || 3000,
		routePrefix: process.env.ROUTE_PREFIX || '',
	},
	database: {
		type: process.env.DB_TYPE || 'mysql',
		host: process.env.DB_HOST || 'localhost',
		port: parseInt(process.env.DB_PORT, 10) || 3306,
		user: process.env.DB_USER || 'root',
		password: process.env.DB_PASSWORD || 'password',
		name: process.env.DB_NAME || 'app',
	},
	smsruToken: process.env.SMSRU_TOKEN,
	jwtSecret: process.env.JWT_SECRET,
})
