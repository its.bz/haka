import { MigrationInterface, QueryRunner } from 'typeorm'

export class users1640831510150 implements MigrationInterface {
	name = 'users1640831510150'

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE IF NOT EXISTS \`user\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(100) NOT NULL, \`phone\` varchar(12) NOT NULL, \`email\` varchar(100) NOT NULL, \`inn\` varchar(20) NOT NULL, UNIQUE INDEX \`inn\` (\`inn\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`DROP INDEX IF EXISTS \`inn\` ON \`user\``)
		await queryRunner.query(`DROP TABLE IF EXISTS \`user\``)
	}
}
