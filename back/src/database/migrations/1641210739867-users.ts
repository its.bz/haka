import { MigrationInterface, QueryRunner } from 'typeorm'

export class users1641210739867 implements MigrationInterface {
	name = 'users1641210739867'

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`DROP INDEX \`inn\` ON \`user\``)
		await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`inn\``)
		await queryRunner.query(
			`ALTER TABLE \`user\` ADD \`smsCode\` varchar(4) NOT NULL`,
		)
		await queryRunner.query(`ALTER TABLE \`user\` ADD \`token\` text NOT NULL`)
		await queryRunner.query(
			`ALTER TABLE \`user\` ADD \`isAdmin\` tinyint NOT NULL DEFAULT 0`,
		)
		await queryRunner.query(
			`ALTER TABLE \`user\` ADD UNIQUE INDEX \`IDX_8e1f623798118e629b46a9e629\` (\`phone\`)`,
		)
		await queryRunner.query(
			`CREATE UNIQUE INDEX \`phone\` ON \`user\` (\`phone\`)`,
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`DROP INDEX \`phone\` ON \`user\``)
		await queryRunner.query(
			`ALTER TABLE \`user\` DROP INDEX \`IDX_8e1f623798118e629b46a9e629\``,
		)
		await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`isAdmin\``)
		await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`token\``)
		await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`smsCode\``)
		await queryRunner.query(
			`ALTER TABLE \`user\` ADD \`inn\` varchar(20) NOT NULL`,
		)
		await queryRunner.query(`CREATE UNIQUE INDEX \`inn\` ON \`user\` (\`inn\`)`)
	}
}
