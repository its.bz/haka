declare const module: any

import { HttpExceptionFilter } from '@/filters/HttpException.filter'

import { config } from './config'
import swaggerInit from '@/swagger'
import { NestFactory } from '@nestjs/core'
import { AppModule } from '@/modules/app/app.module'
import pkg from '../package.json'
import { ValidationPipe } from '@nestjs/common'

async function bootstrap() {
	const app = await NestFactory.create(AppModule)
	app.setGlobalPrefix(config().app.routePrefix)
	app.useGlobalPipes(
		new ValidationPipe({
			disableErrorMessages: false,
			whitelist: true,
			transform: true,
		}),
	)
	app.useGlobalFilters(new HttpExceptionFilter())
	swaggerInit(app)
	app.enableCors()
	await app.listen(config().app.port)

	console.log(`
  ${pkg.name} ver.${pkg.version} by ${pkg.author}
  Started at http://127.0.0.1:${config().app.port}/${config().app.routePrefix}
  NODE_ENV=${config().env}
  `)

	if (module.hot) {
		module.hot.accept()
		module.hot.dispose(() => app.close())
	}
}

bootstrap().catch((e) => {
	throw new Error(e)
})
