import { RequestWithUser, UserDto } from '@/modules/users/dto/user.dto'
import { User } from '@/modules/users/user.entity'

export const fakeUsers: UserDto[] = [
	{
		id: 1,
		name: 'User',
		phone: '+79107373125',
		email: 'dev@its.bz',
		smsCode: '1234',
		isAdmin: false,
		token:
			'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IiIsInBob25lIjoiKzc5MTA3MzczMTI1IiwiZW1haWwiOiIiLCJpbm4iOiIzMTIzMzE4NzUxIiwic21zQ29kZSI6IiIsInRva2VuIjoiMzA5MTE0MjcyNjk5NTgzNzE2MTUxMzgyNDUwMjAwIiwiaWF0IjoxNjQxMDY5NjE3LCJleHAiOjE2NDEwNjk2Nzd9.jVO63Rbr7rRbPJIHQ68XphQkdxqR-szolasG6-ykPPY',
	},
	{
		id: 1,
		name: 'Admin',
		phone: '+79107373125',
		email: 'dev@its.bz',
		smsCode: '1234',
		isAdmin: true,
		token:
			'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IiIsInBob25lIjoiKzc5MTA3MzczMTI1IiwiZW1haWwiOiIiLCJpbm4iOiIzMTIzMzE4NzUxIiwic21zQ29kZSI6IiIsInRva2VuIjoiMzA5MTE0MjcyNjk5NTgzNzE2MTUxMzgyNDUwMjAwIiwiaWF0IjoxNjQxMDY5NjE3LCJleHAiOjE2NDEwNjk2Nzd9.jVO63Rbr7rRbPJIHQ68XphQkdxqR-szolasG6-ykPPY',
	},
]

export const fakeUserRequest: Partial<RequestWithUser> = {
	headers: { Authorization: `Bearer ${fakeUsers[0].token}` },
	user: fakeUsers[0] as User,
}

export const fakeAdminRequest: Partial<RequestWithUser> = {
	headers: { Authorization: `Bearer ${fakeUsers[1].token}` },
	user: fakeUsers[1] as User,
}

export const fakePortfolios = [
	{
		id: 1,
		title: 'Portfolio1 title',
		details: 'Portfolio1 details',
		user_id: 1,
		files: ['portfolio1_1.png', 'portfolio1_2.png', 'portfolio1_3.doc'],
	},
	{
		id: 2,
		title: 'Portfolio2 title',
		details: 'Portfolio2 details',
		user_id: 2,
		files: ['portfolio2_1.png', 'portfolio2_2.png'],
	},
]
