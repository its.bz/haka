import { ConfigModule } from '@nestjs/config'
import { config, DBType } from '@/config'
import { TypeOrmModule } from '@nestjs/typeorm'

export const Config = ConfigModule.forRoot({
	envFilePath: '.env',
	isGlobal: true,
	load: [config],
})

export const DB = TypeOrmModule.forRoot({
	type: config().database.type as DBType,
	host: config().database.host,
	port: config().database.port,
	username: config().database.user,
	password: config().database.password,
	database: config().database.name,
	entities: [__dirname + '/../**/*.entity{.ts,.js}'],
	synchronize: config().env === 'development',
})
