import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common'
import { AuthService } from './auth.service'
import { AuthLoginDto } from '@/modules/auth/dto/auth-login.dto'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { RequestWithUser } from '@/modules/users/dto/user.dto'
import { AuthSendSmsDto } from '@/modules/auth/dto/auth-sendSMS.dto'
import { JwtAuthGuard } from '@/modules/auth/jwt/jwt-auth.guard'

@Controller('auth')
@ApiTags('auth')
export class AuthController {
	constructor(private readonly authService: AuthService) {}

	@Post('/sendSmsCode')
	sendSmsCode(@Body() authSendSMSDto: AuthSendSmsDto) {
		return this.authService.sendSmsCode(authSendSMSDto)
	}

	@Post('/login')
	login(@Body() authLoginDto: AuthLoginDto) {
		return this.authService.login(authLoginDto)
	}

	@Post('/logout')
	@UseGuards(JwtAuthGuard)
	@ApiBearerAuth()
	logout(@Req() req: RequestWithUser) {
		const user = req.user
		return this.authService.logout(user)
	}
}
