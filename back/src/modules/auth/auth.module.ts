import { Module } from '@nestjs/common'
import { AuthService } from './auth.service'
import { AuthController } from './auth.controller'
import { UsersModule } from '@/modules/users/users.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from '@/modules/users/user.entity'
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { config } from '@/config'
import { JwtStrategy } from '@/modules/auth/jwt/jwt.stategy'

@Module({
	imports: [
		TypeOrmModule.forFeature([User]),
		UsersModule,
		PassportModule.register({
			defaultStrategy: 'jwt',
			property: 'user',
			session: false,
		}),
		JwtModule.register({
			secret: config().jwtSecret,
			signOptions: { expiresIn: '6000s' },
		}),
	],
	controllers: [AuthController],
	providers: [AuthService, JwtStrategy],
	exports: [AuthService],
})
export class AuthModule {}
