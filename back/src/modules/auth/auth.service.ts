import axios from 'axios'
import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { AuthLoginDto } from './dto/auth-login.dto'
import { Repository } from 'typeorm'
import { User } from '@/modules/users/user.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { AuthSendSmsDto } from '@/modules/auth/dto/auth-sendSMS.dto'
import { config } from '@/config'
import { UsersService } from '@/modules/users/users.service'
import { JwtService } from '@nestjs/jwt'
import { UserDto } from '@/modules/users/dto/user.dto'

@Injectable()
export class AuthService {
	constructor(
		@InjectRepository(User)
		private userRepository: Repository<User>,
		private userService: UsersService,
		private readonly jwtService: JwtService,
	) {}

	getRandomCode(length = 4) {
		let code = ''
		for (let i = 0; i < length; i++) {
			const rnd = Math.random().toString()
			code += rnd.charAt(2 + Math.floor(Math.random() * rnd.length - 2))
		}
		return code
	}

	async sendSmsCode(sendSmsDto: AuthSendSmsDto) {
		const { phone } = sendSmsDto
		const smsCode = this.getRandomCode()
		const params = {
			api_id: config().smsruToken,
			to: phone,
			msg: `яЗанят: ${smsCode}`,
			json: 1,
		}

		let user = await this.userRepository.findOne({
			where: { phone },
		})
		if (!user) {
			user = await this.userService.create(sendSmsDto)
		}
		const { data } = await axios.get('https://sms.ru/sms/send', { params })
		if (data.status === 'OK') {
			user.smsCode = smsCode
			await user.save()
		} else return { sendSMS: 'error', error: data.sms }
		return { sendSMS: 'ok' }
	}

	async login(authLoginDto: AuthLoginDto) {
		const { phone, smsCode } = authLoginDto
		const user = await this.userRepository.findOne({
			where: { phone, smsCode },
		})
		if (!user) return { error: { message: 'Неверный телефон или код из SMS' } }
		user.smsCode = ''
		user.token = this.jwtService.sign(JSON.parse(JSON.stringify(user)))
		await user.save()
		return { ...user, token: user.token }
	}

	async logout(user: User) {
		user.token = ''
		await user.save()
		return { logout: 'ok' }
	}

	async validateUser(payload: UserDto): Promise<User> {
		const user = await this.userService.findOne({ id: payload.id })
		if (!user) {
			throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED)
		}
		return user
	}
}
