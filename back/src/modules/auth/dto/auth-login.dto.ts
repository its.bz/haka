import { IsMobilePhone, IsNotEmpty } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'

export class AuthLoginDto {
	@ApiProperty({ example: '+79107373125' })
	@IsMobilePhone('ru-RU')
	phone: string

	@ApiProperty({ example: '1234' })
	@IsNotEmpty()
	smsCode: string
}
