import { IsMobilePhone, IsNotEmpty } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'

export class AuthSendSmsDto {
	@ApiProperty({ example: '+79107373125' })
	@IsMobilePhone('ru-RU')
	@IsNotEmpty()
	phone: string
}
