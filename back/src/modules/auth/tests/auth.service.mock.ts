import { AuthService } from '@/modules/auth/auth.service'
import { fakeUsers } from '@/mocks/mockData'

class FakeAuthService {
	async login() {
		return fakeUsers[0]
	}

	async logout() {
		return { ok: true }
	}
}

export const AuthServiceMock = {
	provide: AuthService,
	useValue: new FakeAuthService(),
}
