import { IsMobilePhone, IsNotEmpty } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'

export class CreateUserDto {
	@IsMobilePhone('ru-RU')
	@IsNotEmpty()
	@ApiProperty({ example: '+79107373125' })
	phone: string
}
