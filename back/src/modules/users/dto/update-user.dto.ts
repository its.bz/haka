import { PartialType } from '@nestjs/mapped-types'
import { IsEmail, IsNumberString } from 'class-validator'
import { CreateUserDto } from './create-user.dto'
import { ApiProperty } from '@nestjs/swagger'

export class UpdateUserDto extends PartialType(CreateUserDto) {
	@IsNumberString()
	@ApiProperty({ example: '3123318751' })
	inn: string

	@IsEmail()
	@ApiProperty({ example: 'dev@its.bz' })
	email: string
}
