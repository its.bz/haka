import { Request } from 'express'
import { User } from '@/modules/users/user.entity'

export type UserDto = Partial<User>

export interface RequestWithUser extends Request {
	user: User
}
