import { Test, TestingModule } from '@nestjs/testing'
import { UsersController } from '../users.controller'
import { fakeAdminRequest, fakeUserRequest, fakeUsers } from '@/mocks/mockData'
import { UsersServiceMock } from '@/modules/users/tests/users.service.mock'
import { UpdateUserDto } from '@/modules/users/dto/update-user.dto'
import { RequestWithUser } from '@/modules/users/dto/user.dto'
import { CreateUserDto } from '@/modules/users/dto/create-user.dto'

describe('UsersController', () => {
	let controller: UsersController

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			controllers: [UsersController],
			providers: [UsersServiceMock],
		}).compile()
		controller = module.get<UsersController>(UsersController)
	})

	it('should be defined', () => {
		expect(controller).toBeDefined()
	})

	it('should return all fakeUsers', () => {
		expect(controller.findAll(fakeUserRequest as RequestWithUser)).toBe(
			fakeUsers[0],
		)
	})

	it('should return created user', () => {
		expect(
			controller.create(
				fakeAdminRequest as RequestWithUser,
				fakeUsers[1] as CreateUserDto,
			),
		).toBe(fakeUsers[0])
	})

	it('should return one user', () => {
		expect(controller.findOne(fakeUserRequest as RequestWithUser, 1)).toBe(
			fakeUsers[0],
		)
	})

	it('should return updated user', () => {
		expect(
			controller.update(
				fakeAdminRequest as RequestWithUser,
				1,
				fakeUsers[0] as UpdateUserDto,
			),
		).toBe(fakeUsers[0])
	})

	it('should return deleted user', () => {
		expect(controller.remove(fakeAdminRequest as RequestWithUser, 2)).toBe(
			fakeUsers[0],
		)
	})
})
