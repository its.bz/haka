import { fakeUsers } from '@/mocks/mockData'
import { UsersService } from '@/modules/users/users.service'

class FakeUserService {
	create() {
		return fakeUsers[0]
	}

	findAll() {
		return fakeUsers[0]
	}

	findOne() {
		return fakeUsers[0]
	}

	update() {
		return fakeUsers[0]
	}

	remove() {
		return fakeUsers[0]
	}
}

export const UsersServiceMock = {
	provide: UsersService,
	useValue: new FakeUserService(),
}
