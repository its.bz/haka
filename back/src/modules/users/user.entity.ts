import {
	Column,
	Entity,
	BaseEntity,
	Index,
	PrimaryGeneratedColumn,
} from 'typeorm'
import { IsEmail, IsMobilePhone } from 'class-validator'

@Entity()
@Index('phone', ['phone'], { unique: true })
export class User extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number

	@Column({ length: 100 })
	name: string

	@Column({ length: 12 })
	@IsMobilePhone()
	phone: string

	@Column({ length: 100 })
	@IsEmail()
	email: string

	@Column({ length: 4 })
	smsCode: string

	@Column({ type: 'text' })
	token: string

	@Column({ default: false })
	isAdmin: boolean
}
