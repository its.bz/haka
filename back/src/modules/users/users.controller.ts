import {
	Body,
	Controller,
	Delete,
	Get,
	HttpException,
	HttpStatus,
	Param,
	ParseIntPipe,
	Patch,
	Post,
	Req,
	UseGuards,
} from '@nestjs/common'
import { UsersService } from './users.service'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { JwtAuthGuard } from '@/modules/auth/jwt/jwt-auth.guard'
import { RequestWithUser } from '@/modules/users/dto/user.dto'

@ApiTags('users')
@Controller('users')
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@Post()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	create(@Req() req: RequestWithUser, @Body() createUserDto: CreateUserDto) {
		const user = req.user
		if (user.isAdmin) return this.usersService.create(createUserDto)
		else
			throw new HttpException(
				'Вам нельзя создавать пользователей!',
				HttpStatus.UNAUTHORIZED,
			)
	}

	@Get('me')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	@ApiOperation({
		description: 'Returns current user by JWT in request header',
	})
	getMe(@Req() req: RequestWithUser) {
		const user = req.user
		if (user) return this.usersService.findOne({ id: user.id })
		else throw new HttpException('Токен неверный', HttpStatus.UNAUTHORIZED)
	}

	@Get()
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	findAll(@Req() req: RequestWithUser) {
		const user = req.user
		if (user.isAdmin) return this.usersService.findAll()
		else return this.usersService.findAll({ id: user.id })
	}

	@Get(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	findOne(@Req() req: RequestWithUser, @Param('id', ParseIntPipe) id: number) {
		const user = req.user
		if (user.isAdmin) return this.usersService.findOne({ id })
		else {
			if (user.id === id) return this.usersService.findOne({ id })
			else
				throw new HttpException(
					`Это не ваш ID! Ваш: ${user.id}`,
					HttpStatus.UNAUTHORIZED,
				)
		}
	}

	@Patch(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	update(
		@Req() req: RequestWithUser,
		@Param('id', ParseIntPipe) id: number,
		@Body() updateUserDto: UpdateUserDto,
	) {
		const user = req.user
		if (user.isAdmin) return this.usersService.update({ id }, updateUserDto)
		else {
			if (user.id === id) return this.usersService.update({ id }, updateUserDto)
			else throw new HttpException('Это не ваш ID!', HttpStatus.UNAUTHORIZED)
		}
	}

	@Delete(':id')
	@ApiBearerAuth()
	@UseGuards(JwtAuthGuard)
	remove(@Req() req: RequestWithUser, @Param('id', ParseIntPipe) id: number) {
		const user = req.user
		if (user.isAdmin) return this.usersService.remove(id)
		else {
			if (user.id === id) return this.usersService.remove(+id)
			else throw new HttpException('Это не ваш ID!', HttpStatus.UNAUTHORIZED)
		}
	}
}
