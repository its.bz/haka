import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { Repository, TypeORMError } from 'typeorm'
import { User } from '@/modules/users/user.entity'
import { InjectRepository } from '@nestjs/typeorm'

@Injectable()
export class UsersService {
	constructor(
		@InjectRepository(User)
		private userRepository: Repository<User>,
	) {}

	async create(createUserDto: CreateUserDto) {
		const result = await this.userRepository.insert(createUserDto)
		if (result.raw.insertId)
			return await this.userRepository.findOne({
				where: { id: result.raw.insertId },
			})
		throw new HttpException('User already exists', HttpStatus.BAD_REQUEST)
	}

	findAll(options: object = {}) {
		return this.userRepository.find(options)
	}

	findOne(options: object) {
		return this.userRepository.findOne(options)
	}

	async update(options: object = {}, updateUserDto: UpdateUserDto) {
		try {
			const result = await this.userRepository.update(options, updateUserDto)
			if (result.raw.insertId)
				return await this.userRepository.findOne({
					where: { id: result.raw.insertId },
				})
		} catch (e) {
			throw new TypeORMError(e)
		}
	}

	async remove(id: number) {
		try {
			const result = await this.userRepository.delete(id)
			if (result.raw.deleteId) return { deleted: true }
		} catch (e) {
			throw new TypeORMError(e)
		}
	}
}
