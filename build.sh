#!/bin/bash

# Cleanup
#echo "==> Cleaning up dist.."
#rm -f `find ./dist/ ! -path "*node_modules*"`

# Load libs
echo "==> Loading node_modules an build.."
NODE_ENV=production
( cd front && yarn install --production=false && yarn build:pwa --production=true )
( cd back && yarn install --production=false && yarn build )

# Remove map-files if needed
#rm -rf ./dist/js/*.map
echo "==> Bundle complete! <=="
