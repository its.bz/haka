# ITS :: Fullstack boilerplate

[![timesheet](https://wakatime.com/badge/user/b63eae7a-adaa-4419-a51e-0f59a1a4a46f/project/72a81e3f-2cc0-47f3-a0ea-5cce071a2965.svg)](https://wakatime.com/projects/haka.its.bz)
[![pipeline status](https://gitlab.com/its.bz/haka/badges/main/pipeline.svg)](https://gitlab.com/its.bz/haka/-/commits/main)
[![coverage report](https://gitlab.com/its.bz/haka/badges/main/coverage.svg)](https://gitlab.com/its.bz/haka/-/commits/main)

Site and mobile application (PWA+cordova) for RK7.CRM by UCS

## Themes

This app may be customized for different brands and builds simultaneously in different ```dist/$theme``` path.\
When build are done you can deploy an accorded path to accorded server to serve it.\
Or use CI for deploy.

## Architecture

App consist of backend part nd frontend part.\
Backend serves front on port on development and from directory as static.\
There is no additional server needed to serve frontend.

## Production

```shell
# Serve app using PM2 supervisor
yarn global add pm2

# Set theme that you want to build
theme=<theme>
./build.sh $theme

# Start application
cd dist/$theme
pm2 start ecosystem.config.js
```

## Development

```shell
(
  cd front && yarn install && yarn dev
)
(
  cd back && yarn install && yarn dev
)
```

## Docker
You can build docker image using ```Dockerfile``` file and then use it:
```shell
cd dist/<theme>
sudo docker build -t <tag_here> .
docker run -p <your_port>:4100 -v <your_logs_dir>:/app/back/logs/ <tag_here>
```
example:
```shell
cd dist/ostrov
sudo docker build -t osis_crm-ostrov .
# on Linux
docker run -p 4101:4100 -v $(pwd)/logs/:/app/back/logs/ osis_crm-ostrov
# on Windows
docker run -p 4101:4100 -v ./logs/:/app/back/logs/ osis_crm-ostrov
```

## Release
Use ```release.sh``` script to:
  - Increment patch block in package versions
  - Commit changes, tag commit (need for properly create changelog)
  - Update changelog in ```README.md``` file and amment commit with new changes 
  - Delete previous tag and tag last commit

For now you can check is all OK and then push changes to remotes with:
```shell
git push origin
```

## Contributing

Project is fully proprietary and not open sourced. Nobody contribute but ITS company employees.

## Bugs

Submit bugs [here](https://gitlab.com/ostrov.team/ostrovis/osis_crm/issues)

## History

```
{{#each releases}}{{title}} @ {{niceDate}}{{#commits}}  
  - {{subject}}{{/commits}}

{{/each}}
```

## Credits

Contact us: [dev@its.bz](mailto:dev@its.bz)\
Our site: [its.bz](https://its.bz)

## License

Proprietary
