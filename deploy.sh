#!/bin/bash

# === ENV
# DEST_PATH: путь, куда вываливать файлы на целевом сервере
DEST_HOST=its@its.bz

# Making release
if [ "$CI_COMMIT_TAG" ]; then
  tar -cvzf release-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA.tgz ./dist
  mv release-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA.tgz dist/front/release-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA.tgz
  curl --header "Content-Type:application/json" \
    --header "PRIVATE-TOKEN:pSx8tayz52xya3qTcfUL" \
    --data "{\"name\":\"Project $CI_PROJECT_NAME Release $CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA\",\"tag_name\":\"$CI_COMMIT_TAG\",\"ref\":\"$CI_COMMIT_REF_NAME\",\"description\":\"Release-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA\",\"assets\":{\"links\":[{\"name\":\"release-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA.tgz\", \"url\":\"https://crm.ostrov.team/release-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA.tgz\"}]}}" \
    --request POST https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases
fi

# Deploy
sshpass -V
ls -lahs dist
export SSHPASS=$USER_PASS
sshpass -e rsync -rav -e 'ssh -p 31666 -o StrictHostKeyChecking=no' --delete dist $DEST_HOST:"$DEST_PATH"
echo Site with deployed successfully to "$DEST_PATH"

if [ "$CI_COMMIT_TAG" ]; then
  curl -X GET "https://api.its.bz/v2/telegramSend/-403044943/$CI_PROJECT_NAME ($CI_COMMIT_REF_NAME) is deployed?access-token=4pNVMVjhCAfXBgOLSMl84xyp8d663pDV" \
    --insecure
fi
