module.exports = {
    apps: [
        {
            name: 'haka-test-back',
            script: 'dist/src/main.js',
            cwd: 'back',
            watch: ["back/dist"],
            ignore_watch: ['logs', 'node_modules'],
            instances: 1,
            autorestart: true,
            //exec_mode: 'cluster',
            exec_mode: 'fork',
            max_memory_restart: '1G',
            args: '',
            node_args: '',
			merge_logs: true,
            output: './logs/access.log',
            error: './logs/error.log',
            env: {
                NODE_ENV: 'production',
				PORT: 5001,
				NODE_APP_INSTANCE: 'production'
            }
        },
        {
            name: 'haka-test-front',
            script: "serve",
            cwd: 'front',
            watch: ["dist"],
			instances: 1,
            autorestart: true,
            exec_mode: 'fork',
            env: {
                PM2_SERVE_PATH: 'dist/pwa',
                PM2_SERVE_PORT: 5002,
                PM2_SERVE_SPA: 'true',
                PM2_SERVE_HOMEPAGE: '/index.html'
            }
        }
    ]
}
