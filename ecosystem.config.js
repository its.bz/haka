module.exports = {
    apps: [
        {
            name: 'haka-back',
            script: 'dist/src/main.js',
            cwd: 'back',
            watch: ["back/dist"],
            ignore_watch: ['logs', 'node_modules'],
            instances: 10,
            autorestart: true,
            exec_mode: 'cluster',
            //exec_mode: 'fork',
            max_memory_restart: '1G',
            args: '',
            node_args: '',
			merge_logs: true,
            output: './logs/access.log',
            error: './logs/error.log',
            env: {
                NODE_ENV: 'production',
				PORT: 6001,
				NODE_APP_INSTANCE: 'production'
            }
        },
        {
            name: 'haka-front',
            script: "serve",
            cwd: 'front',
            watch: ["dist"],
			instances: 10,
            autorestart: true,
            exec_mode: 'cluster',
            env: {
                PM2_SERVE_PATH: 'dist/pwa',
                PM2_SERVE_PORT: 6002,
                PM2_SERVE_SPA: 'true',
                PM2_SERVE_HOMEPAGE: '/index.html'
            }
        }
    ]
}
