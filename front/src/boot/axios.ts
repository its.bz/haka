import { boot } from 'quasar/wrappers'
import axios, { AxiosInstance } from 'axios'
import { App } from '@vue/runtime-core'
import { Store } from 'vuex'
import { StateInterface } from '@/store'

class Api {
  private app
  private store
  private apiUrl: string
  private ax: AxiosInstance

  constructor(app: App<any>, store: Store<StateInterface>, apiUrl: string) {
    this.app = app
    this.store = store
    this.apiUrl = apiUrl
    this.ax = axios.create({ baseURL: apiUrl })
  }

  async post(path: string, postData?: object, options?: object) {
    try {
      const { data } = await this.ax.post(path, postData, {
        ...options,
      })

      if (data.error) {
        await this.store.dispatch('common/error', data.error, { root: true })
        return data
      }
      return data
    } catch (e: any) {
      await this.store.dispatch('common/error', e, { root: true })
      return false
    }
  }

  async get(path: string, params?: object, options?: object) {
    try {
      const { data } = await this.ax.get(path, {
        params,
        ...options,
      })
      if (data.error) {
        await this.store.dispatch('common/error', data.error, { root: true })
        return data
      }
      return data
    } catch (e: any) {
      await this.store.dispatch('common/error', e, { root: true })
      return false
    }
  }
}

const req: { api: Api } = { api: {} as Api }

export default boot(({ app, store }) => {
  const apiUrl =
    process.env.NODE_ENV === 'production' ? '/api' : 'http://localhost:6001/api'
  req.api = new Api(app, store, apiUrl)

  // for use inside Vue files (Options API) through this.$axios and this.$api
  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = req.api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
})

export const api = () => req.api
