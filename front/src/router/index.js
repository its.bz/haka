import { route } from 'quasar/wrappers'
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from 'vue-router'
import routes from './routes'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(({ store }) => {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
    ? createWebHistory
    : createWebHashHistory

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(
      process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE,
    ),
  })
  Router.beforeEach((to, from, next) => {
    /** Authenticated
     * at routes: /login and /
     * Redirect to /card
     * */
    if (store.state?.user?.user?.token && to.path === '/')
      return next('/account')

    /**
     * User not Authenticated
     * at all protected routes (meta.requiresAuth) except /
     * Redirect to /
     * */
    if (
      !store.state?.user?.user?.token &&
      to.matched.some((record) => record.meta.requiresAuth)
    )
      return next('/')

    return next()
  })

  return Router
})
