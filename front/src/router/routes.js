const routes = [
  {
    path: '/',
    component: () => import('layouts/GuestLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/about', component: () => import('pages/docs/About.vue') },
      { path: '/partners', component: () => import('pages/docs/Partners.vue') },
      { path: '/offer', component: () => import('pages/docs/Offer.vue') },
      {
        path: '/private-data',
        component: () => import('pages/docs/PrivateData.vue'),
      },
      { path: '/void', component: () => import('pages/docs/Void.vue') },
      { path: '/faq', component: () => import('pages/docs/Faq.vue') },
      { path: '/support', component: () => import('pages/docs/Support.vue') },
      { path: '/contact', component: () => import('pages/docs/Contact.vue') },
      {
        path: '/cv/:catchAll(.*)*',
        component: () => import('pages/worker/Profile.vue'),
      },
    ],
  },
  {
    path: '/account',
    component: () => import('layouts/WorkerLayout.vue'),
    meta: { requiresAuth: true },
    children: [
      {
        path: '',
        component: () => import('pages/worker/Profile.vue'),
        meta: { title: 'Мой профиль' },
      },
      {
        path: 'orders',
        component: () => import('pages/worker/Orders.vue'),
        meta: { title: 'Заказы', requiresAuth: true },
      },
      {
        path: 'clients',
        component: () => import('pages/worker/Clients.vue'),
        meta: { title: 'Клиенты', requiresAuth: true },
      },
      {
        path: 'calendar',
        component: () => import('pages/worker/Calendar.vue'),
        meta: { title: 'Календарь', requiresAuth: true },
      },
      {
        path: 'messages',
        component: () => import('pages/worker/Messages.vue'),
        meta: { title: 'Общение' },
        requiresAuth: true,
      },
      {
        path: 'qrcode',
        component: () => import('pages/worker/QRCode.vue'),
        meta: { title: 'QR' },
      },
    ],
  },

  {
    path: '/cv/:catchAll(.*)*',
    component: () => import('pages/worker/Profile.vue'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
]

export default routes
