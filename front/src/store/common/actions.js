import { Notify } from 'quasar'

export function error(context, err) {
  let msg = err.toString()
  if (err?.response?.data?.error?.message) msg = err.response.data.error.message
  else if (err?.response?.data?.error) msg = err.response.data.error.toString()
  else if (err.message) msg = err.message

  Notify.create({
    type: 'negative',
    message: 'Ошибка',
    caption: msg,
  })
}

export function message(context, opts) {
  Notify.create({
    type: 'positive',
    ...opts,
  })
}

export function setTitle(context, payload) {
  context.commit('setTitle', payload)
}
