import { store } from 'quasar/wrappers'
import { createStore, Store as VuexStore, useStore as vuexUseStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import { InjectionKey } from 'vue'
import common from '@/store/common'
import user from './user'
import settings from './settings'
import orders from './orders'
import { UserState } from '@/store/user/state'

export interface StateInterface {
  user: UserState
}

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: VuexStore<StateInterface>
  }
}

export const storeKey: InjectionKey<VuexStore<StateInterface>> =
  Symbol('vuex-key')

export default store(() =>
  createStore({
    modules: {
      common,
      user,
      settings,
      orders,
    },
    plugins: [
      createPersistedState({
        paths: ['user', 'settings'],
      }),
    ],

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: !!process.env.DEBUGGING,
  }),
)

export function useStore() {
  return vuexUseStore(storeKey)
}
