export default () => ({
  orders: [
    {
      _id: 1431,
      createdAt: '2021-12-17',
      client: {
        ul: '"ИТС", ООО',
        inn: '3123318751',
      },
      work: {
        title: 'Починить принтер',
        details: 'МФУ Xerox 3220 не печатает совсем.',
      },
      state: 'closed',
    },
    {
      _id: 1432,
      createdAt: '2021-12-18',
      client: {
        ul: '"ИТС", ООО',
        inn: '3123318751',
      },
      work: {
        title: 'Починить другой принтер',
        details: 'МФУ Xerox 3210 не печатает совсем тоже.',
      },
      state: 'unpaid',
    },
  ],
})
