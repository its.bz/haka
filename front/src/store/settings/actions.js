export function setDarkMode(context, payload) {
  context.commit('setDarkMode', payload)
}

export function toggleDarkMode({ commit, state }) {
  commit('setDarkMode', !state.darkMode)
}
