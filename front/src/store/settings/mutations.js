import { Dark } from 'quasar'

export function setDarkMode(state, payload) {
  state.darkMode = payload
  Dark.set(state.darkMode)
}
