import { ActionTree } from 'vuex'
import { api } from '@/boot/axios'
import { UserState } from '@/store/user/state'
import { StateInterface } from '@/store'

const actions: ActionTree<UserState, StateInterface> = {
  setState(context, payload) {
    context.commit('setState', payload)
  },

  async sendSMSCode(context, { phone }) {
    return await api().post('/auth/sendSmsCode', { phone })
  },

  async login(context, { phone, smsCode }) {
    const res = await api().post('/auth/login', { phone, smsCode })
    if (!res || res.error) return false
    context.commit('setState', res)
    return true
  },

  async logout(context, { vm }) {
    const res = await api().post('/auth/logout')
    if (!res && res.error) return false
    context.commit('setState', false)
    await vm.$router.replace('/')
    // window.location.href = '/'
    return true
  },
}

export default actions
