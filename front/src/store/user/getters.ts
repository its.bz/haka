import { GetterTree } from 'vuex'
import { StateInterface } from '@/store'
import { UserState } from '@/store/user/state'

const getters: GetterTree<UserState, StateInterface> = {
  someAction(/* context */) {
    // your code
  },
}

export default getters
