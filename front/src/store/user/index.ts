import { Module } from 'vuex'
import state, { UserState } from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
import { StateInterface } from '@/store'

const userModule: Module<UserState, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
}

export default userModule
