import { MutationTree } from 'vuex'
import { UserState } from './state'

const mutation: MutationTree<UserState> = {
  setState(state, payload: any) {
    if (!payload) state.user = undefined
    state.user = { ...state.user, ...payload }
  },
}

export default mutation
