export interface UserInner {
  id: number
  phone: string
  email?: string
  token: string
  isAdmin: boolean
  profile?: {
    name: string
    surname: string
    middle_name: string
  }
}

export interface UserState {
  user?: UserInner
}

function state(): UserState {
  return { user: {} as UserInner }
}

export default state
