#!/usr/bin/env bash
# Запускаем этот скрипт когда нужно develop-ветку выкатить на релиз

yarn itsbz-onever i patch
newVer=$(node_modules/.bin/itsbz-onever get --machine)
git checkout .onever.json
git flow release start "$newVer"
yarn itsbz-onever set "$newVer" && yarn itsbz-onever apply
(cd back && yarn format && yarn && yarn lint)
git add .
git commit --amend --no-edit
git tag "$newVer"
yarn changelog
git add --ignore-errors -A -f -- README.md
git commit --amend --no-edit
git tag -d "$newVer"
export GIT_MERGE_AUTOEDIT=no
git flow release finish "$newVer" -m "$newVer" -T "$newVer"
unset GIT_MERGE_AUTOEDIT
#git rebase main -f
git push origin main --progress --tags
git push origin test --progress
